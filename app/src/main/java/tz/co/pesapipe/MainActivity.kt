package tz.co.pesapipe

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.*
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import tz.co.pesapipe_sdk.data.PaymentMessage
import tz.co.pesapipe_sdk.service.PaymentsAckService

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkReceiveSmsPermission()
        startService(Intent(applicationContext, PaymentsAckService::class.java))
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            receiveSmsPermissionCode -> {
                // Send SMS
                if (permissions[0].equals(Manifest.permission.RECEIVE_SMS, true) &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(tag, getString(R.string.debug_receive_sms_perm_granted))
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun checkReceiveSmsPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            Log.d(tag, getString(R.string.debug_receive_sms_perm_denied))
            ActivityCompat.requestPermissions(this, arrayOf<String>(android.Manifest.permission.RECEIVE_SMS), receiveSmsPermissionCode)
        } else {
            Log.d(tag, getString(R.string.debug_receive_sms_perm_granted))
        }
    }

    companion object {
        private const val receiveSmsPermissionCode = 0x227
        private val tag: String = MainActivity::class.java.simpleName
    }
}
