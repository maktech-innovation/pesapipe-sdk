package tz.co.pesapipe_sdk.dao

import androidx.room.*
import tz.co.pesapipe_sdk.data.PaymentVendor

@Dao
interface PaymentVendorDao {
    @Query("SELECT * FROM tbl_payment_vendors")
    fun findAll() : List<PaymentVendor>

    @Query("SELECT * FROM tbl_payment_vendors WHERE id IN (:paymentVendorIds)")
    fun loadAllByIds(paymentVendorIds: IntArray): List<PaymentVendor>

    @Query("SELECT * FROM tbl_payment_vendors WHERE id")
    fun findById(id: Int): PaymentVendor?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(paymentVendors: List<PaymentVendor>)

    @Delete
    fun delete(paymentVendor: PaymentVendor)
}