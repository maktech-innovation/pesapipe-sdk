package tz.co.pesapipe_sdk.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import tz.co.pesapipe_sdk.data.PaymentMessage

@Dao
interface PaymentMessageDao {
    @Query("SELECT * FROM tbl_payment_messages")
    fun getAll(): List<PaymentMessage>

    @Query("SELECT * FROM tbl_payment_messages WHERE message_id IN (:messageIds)")
    fun loadAllByIds(messageIds: IntArray): List<PaymentMessage>

    @Query("SELECT * FROM tbl_payment_messages WHERE vendor_mcc=:mcc AND vendor_mnc=:mnc")
    fun findByMccMnc(mcc: Int, mnc: Int): PaymentMessage?

    @Insert
    fun insertAll(vararg messages: PaymentMessage)

    @Delete
    fun delete(message: PaymentMessage)
}
