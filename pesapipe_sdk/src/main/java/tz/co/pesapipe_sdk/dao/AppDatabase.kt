package tz.co.pesapipe_sdk.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import tz.co.pesapipe_sdk.data.PaymentMessage
import tz.co.pesapipe_sdk.data.PaymentVendor
import tz.co.pesapipe_sdk.util.DATABASE_NAME
import tz.co.pesapipe_sdk.workers.SeedDatabaseWorker


@Database(entities = [PaymentMessage::class, PaymentVendor::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun paymentMessagesDao() : PaymentMessageDao
    abstract fun paymentVendorsDao() : PaymentVendorDao

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: AppDatabase? = null

        // For singleton instantiation
        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .addCallback(
                    object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            val request = OneTimeWorkRequestBuilder<SeedDatabaseWorker>().build()
                            WorkManager.getInstance(context).enqueue(request)
                        }
                    }
                ).build()
        }

    }
}
