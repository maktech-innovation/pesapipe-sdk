package tz.co.pesapipe_sdk.data

data class PaymentConfig(val vendor: PaymentVendor, val integrationId: String)
