package tz.co.pesapipe_sdk.data

import android.telephony.SmsMessage
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.util.*

@Entity(
    tableName = "tbl_payment_messages",
    foreignKeys = [],
    indices = [Index("message_id")]
)
data class PaymentMessage(
    /**
     * Unique message id
     */
    @ColumnInfo(name = "message_id") val messageId: Int,
    /**
     * Client MSISDN - phone number of an end which establishes
     * payments, usually this phone. This is obtained through OTP verification
     */
    @ColumnInfo(name = "client_msisdn") val clientMsisdn: String,
    /**
     * Receiver MSISDN - phone number of an end which receives
     * transactions
     */
    @ColumnInfo(name = "receiver_msisdn") val receiverMsisdn: String,
    /**
     * Message received from network operator.
     */
    @ColumnInfo(name = "message") val message: String,
    /**
     * Vendor MCC - Mobile Country Code of the vendor
     */
    @ColumnInfo(name = "vendor_mcc") val vendorMcc: Int,
    /**
     * Vendor MNC - Mobile Network Code of the vendor
     */
    @ColumnInfo(name = "vendor_mnc") val vendorMnc: Int,
    /**
     * Vendor name - Name of the vendor
     */
    @ColumnInfo(name = "vendor_mnc") val vendorName: String,
    /**
     * Integration ID
     */
    @ColumnInfo(name = "integration") val integrationId: String,
    /**
     * Date at which the transaction was received.
     */
    val receivedAt: Calendar = Calendar.getInstance(),
) {
    class Deserializer : ResponseDeserializable<PaymentMessage> {
        override fun deserialize(content: String): PaymentMessage? {
            return Gson().fromJson(content, PaymentMessage::class.java)
        }
    }

    companion object {
        fun createFromSmsMessage(smsMessage: MutableMap.MutableEntry<Int, List<SmsMessage>>) = PaymentMessage(
            integrationId = "60b76bd4f813e28275c8ef48",
            vendorName = "M-PESA",
            vendorMcc = 640,
            vendorMnc = 4,
            message = smsMessage.value.joinToString(" ") { b -> b.displayMessageBody },
            clientMsisdn = smsMessage.value[0].displayOriginatingAddress,
            receiverMsisdn = "255743684072",
            receivedAt = Calendar.getInstance(),
            messageId = 20020,
        )
    }
}
