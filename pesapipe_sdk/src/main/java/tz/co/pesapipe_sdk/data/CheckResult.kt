package tz.co.pesapipe_sdk.data

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

data class CheckResult(
    val taskId: Int,
    val messageId: String,
    val status: String,
) {
    class Deserializer : ResponseDeserializable<CheckResult> {
        override fun deserialize(content: String): CheckResult? {
            return Gson().fromJson(content, CheckResult::class.java)
        }
    }
}