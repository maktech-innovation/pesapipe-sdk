package tz.co.pesapipe_sdk.service

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.*
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tz.co.pesapipe_sdk.R
import tz.co.pesapipe_sdk.data.PaymentMessage
import tz.co.pesapipe_sdk.receivers.NewSmsBroadcastReceiver

/**
 * Receive and acknowledge Mobile money transactions received via SMSes by
 * establishing communication with an online server.
 */
class PaymentsAckService : Service(), NewSmsBroadcastReceiver.OnSmsItemsReceivedCallback {

    /*
     * The service will be running in a background thread. Normally, a service
     * is dispatched in the same thread as the component launching it. But
     * it is best practise to isolate services and dispatch them independently
     */
    private var serviceLooper: Looper? = null
    private var serviceHandler: ServiceHandler? = null

    /*
     * A messenger for our service is used to communicate with other messangers from
     * other external components
     */
    private lateinit var messenger: Messenger

    /*
     * A broadcast receiver for the new messages. The receiver is configured
     * via IPC API
     */
    private lateinit var smsReceiver: NewSmsBroadcastReceiver

    private var isServiceStarted: Boolean = false;

    private lateinit var wakeLock: PowerManager.WakeLock;

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    override fun onBind(intent: Intent): IBinder? {
        return messenger.binder
    }

    override fun onCreate() {
        super.onCreate()
        smsReceiver =
            NewSmsBroadcastReceiver(this)

        val filter = IntentFilter().also { intentFilter ->
            intentFilter
                .addAction("android.provider.Telephony.SMS_RECEIVED")
        }

        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread("PaymentsAckHandler", Process.THREAD_PRIORITY_BACKGROUND).apply {
            start()
            // Get the thread's looper and use it for our handler
            serviceLooper = looper
            serviceHandler = ServiceHandler(looper)
            messenger = Messenger(serviceHandler)
        }

        registerReceiver(smsReceiver, filter)
        val notification = createNotification()
        startForeground(1, notification)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            when (intent.action) {
                Actions.START.name -> startService()
                Actions.STOP.name -> stopService()
            }
        } else {
            Log.d(PaymentsAckService::class.simpleName, "With null intent, probably the service is running")
        }

        return START_STICKY
    }

    override fun onDestroy() {
        unregisterReceiver(smsReceiver)
        // notificationManager.cancel(notificationsId)
        super.onDestroy()
    }

    override fun onSmsItemsReceived(smsItems: List<PaymentMessage>) {
        Log.d(PaymentsAckService::class.simpleName, smsItems.toString())
        // Start a work to save payment
        // Then notify the result to the bound components
        if (clients.isEmpty()) {
            // Show notification
        } else {
            // Send transaction to the main component
        }
    }

    private fun startService() {
        if (isServiceStarted) return
        isServiceStarted = true
        setServiceState(this, ServiceState.STARTED)

        // We need this in order to make sure our service is not affected by DOZE mode
        wakeLock = (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
            newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SmsService::lock").apply {
                acquire(10*60*1000L /*10 minutes*/)
            }
        }

        // We're starting a loop in a coroutine
        GlobalScope.launch(Dispatchers.IO) {
            while(isServiceStarted) {
                launch(Dispatchers.IO) {
                    // pingFakeServer()
                }

                delay(1 * 60 * 1000)
            }
        }
    }

    private fun stopService() {
        Log.d(PaymentsAckService::class.simpleName, "Service is going to stop")
        try {
            wakeLock?.let {
                if (it.isHeld) {
                    it.release()
                }
                stopForeground(true)
                stopSelf()
            }
        } catch (e: Exception) {
            Log.e(PaymentsAckService::class.simpleName, "An attempt to stop the service which is not started")
        }
        isServiceStarted = false
        setServiceState(this, ServiceState.STOPPED)
    }

    private fun createNotification(): Notification {
        val notificationChannelId = "Sms Sync Service"

        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                notificationChannelId,
                "Creating SMS is an easy task",
                NotificationManager.IMPORTANCE_HIGH
            ).let {
                it.description = "Hi there, it is very easy to create an SMS receiving app"
                it.enableLights(true)
                it.lightColor = Color.RED
                it.enableVibration(true)
                it.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                it
            }
            notificationManager.createNotificationChannel(notificationChannel)
        }

        // val pendingIntent: PendingIntent = Intent(this, MainActivity::class.java)
        @Suppress("DEPRECATION")
        val builder: Notification.Builder = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) Notification.Builder(this, notificationChannelId) else Notification.Builder(this)

        @Suppress("DEPRECATION")
        return builder
            .setContentTitle("Notification")
            .setContentText("Notification Sample")
            .setTicker("Ticker text")
            .setPriority(Notification.PRIORITY_HIGH)
            .setSmallIcon(R.drawable.ic_android_black_24dp)
            .build()
    }

    /*
     * Handler class that receives messages from the thread
     */
    private inner class ServiceHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            when(msg.what) {
                MSG_REGISTER_RECEIVER -> clients = clients + msg.replyTo
                MSG_UNREGISTER_RECEIVER -> clients = clients - msg.replyTo
                else -> super.handleMessage(msg)
            }
        }
    }

    companion object {
        /**
         * Register client (receiver)
         */
        const val MSG_REGISTER_RECEIVER = 1

        /**
         * Unregister client (remove)
         */
        const val MSG_UNREGISTER_RECEIVER = 2

        /**
         * Acknowledge when we receive transaction
         */
        const val MSG_TRANSACTION_ACK = 3

        /**
         * Negative acknowledgement when the transaction received is not
         * valid.
         */
        const val MSG_TRANSACTION_NACK = 4

        /**
         * A list of clients
         */
        var clients: List<Messenger> = listOf()
    }
}
