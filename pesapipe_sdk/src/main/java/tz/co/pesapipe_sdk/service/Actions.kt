package tz.co.pesapipe_sdk.service

enum class Actions {
    START,
    STOP
}