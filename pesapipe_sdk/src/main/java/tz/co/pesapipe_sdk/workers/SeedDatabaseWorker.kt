package tz.co.pesapipe_sdk.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import kotlinx.coroutines.coroutineScope
import tz.co.pesapipe_sdk.dao.AppDatabase
import tz.co.pesapipe_sdk.data.PaymentVendor
import tz.co.pesapipe_sdk.util.VENDORS_DATA_FILE
import java.lang.Exception

class SeedDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun doWork(): Result = coroutineScope {
        try {
            applicationContext.assets.open(VENDORS_DATA_FILE).use { inputStream ->
                JsonReader(inputStream.reader()).use { jsonReader ->
                    val paymentVendorType = object : TypeToken<List<PaymentVendor>>() {}.type
                    val paymentVendorList = Gson().fromJson<List<PaymentVendor>>(jsonReader, paymentVendorType)
                    val database = AppDatabase.getInstance(applicationContext)
                    // Seed vendors
                    database.paymentVendorsDao().insertAll(paymentVendorList)
                }
            }
            Result.success()
        } catch (e: Exception) {
            Log.e(TAG, e.toString());
            Result.failure()
        }
    }

    companion object {
        private const val TAG = "SeedDatabaseWorker"
    }
}
