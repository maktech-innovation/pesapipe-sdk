package tz.co.pesapipe_sdk.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.google.gson.Gson
import kotlinx.coroutines.coroutineScope
import tz.co.pesapipe_sdk.data.PaymentMessage
import java.lang.Exception

class PostPaymentAckWorker(context: Context, workerParams: WorkerParameters) : CoroutineWorker(context, workerParams) {
    override suspend fun doWork(): Result = coroutineScope {
        try {
            val data = inputData.keyValueMap
            Result.success()
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
            Result.failure()
        }
    }

    companion object {
        const val TAG = "PostPaymentAckWorker"
    }

    private fun postPayment(paymentMessages: List<PaymentMessage>) {
        val jsonBody = Gson().toJson(paymentMessages)
        try {
            Fuel.post("/payments", listOf("operation" to "checker"))
                .jsonBody(jsonBody)
                .responseObject(PaymentMessage.Deserializer()) { _, _, result ->
                    val (model, error) = result
                    if (model != null) {
                        // Save transaction to the database.
                    }
                }
        } catch (e: Exception) {

        }
    }
}