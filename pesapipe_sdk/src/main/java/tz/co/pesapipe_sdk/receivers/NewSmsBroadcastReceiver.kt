package tz.co.pesapipe_sdk.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage
import android.util.Log
import tz.co.pesapipe_sdk.R
import tz.co.pesapipe_sdk.data.PaymentMessage
import java.util.*
import kotlin.collections.ArrayList

class NewSmsBroadcastReceiver(private val onSmsItemsReceivedCallback: OnSmsItemsReceivedCallback) :
    BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val messages = ArrayList<SmsMessage>()
        val extras = intent.extras

        Log.d(NewSmsBroadcastReceiver::class.simpleName, context.getString(R.string.debug_sms_received))

        if (extras != null) {
            val pdus = extras.get(extrasType) as Array<*>
            for (extra in pdus) {
                messages.add(getIncomingMessage(extra, extras))
            }
        }
        // process SMSes
        processSmsMessages(messages)
    }

    @Suppress("DEPRECATION")
    private fun getIncomingMessage(msg: Any?, bundle: Bundle?): SmsMessage {
        return if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            val fmt = bundle?.getString(smsFormatKey)
            SmsMessage.createFromPdu(msg as ByteArray, fmt)
        } else {
            SmsMessage.createFromPdu(msg as ByteArray)
        }
    }

    private fun processSmsMessages(messages: List<SmsMessage>) {
        val map = WeakHashMap<Int, List<SmsMessage>>()
        for (message in messages) {
            var list: List<SmsMessage> =
                if (map[message.indexOnIcc] != null)
                    map[message.indexOnIcc]!!
                else listOf()
            list = list + message
            map[message.indexOnIcc] = list
        }
        // Convert mapped messages into a list
        var smsModels = listOf<PaymentMessage>()
        for (message in map) {
            val model = PaymentMessage.createFromSmsMessage(message)
            smsModels = smsModels + model
        }

        onSmsItemsReceivedCallback.onSmsItemsReceived(smsModels)
    }


    companion object {
        private const val extrasType = "pdus"
        private const val smsFormatKey = "format"
    }

    interface OnSmsItemsReceivedCallback {
        fun onSmsItemsReceived(smsItems: List<PaymentMessage>)
    }
}
