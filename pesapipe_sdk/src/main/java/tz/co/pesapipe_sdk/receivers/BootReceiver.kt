package tz.co.pesapipe_sdk.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import tz.co.pesapipe_sdk.service.Actions
import tz.co.pesapipe_sdk.service.ServiceState
import tz.co.pesapipe_sdk.service.PaymentsAckService
import tz.co.pesapipe_sdk.service.getServiceState

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == Intent.ACTION_BOOT_COMPLETED && getServiceState(context) == ServiceState.STARTED) {
            Intent(context, PaymentsAckService::class.java).also {
                it.action = Actions.START.name
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Log.d(BootReceiver::class.simpleName ,"Starting the service in >=26 Mode from a BroadcastReceiver")
                    context.startForegroundService(it)
                    return
                }
                Log.d(BootReceiver::class.simpleName,"Starting the service in < 26 Mode from a BroadcastReceiver")
                context.startService(it)
            }
        }
    }
}
